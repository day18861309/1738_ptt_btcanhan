﻿using LapTrinhWebBuoi7.Models;
using Microsoft.EntityFrameworkCore;

namespace LapTrinhWebBuoi7.DataAccess
{
    public class ApplicationDBContext :DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext>
        options) : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transactions> transactions { get; set; }
        public DbSet<Employees> employees { get; set; }
        public DbSet<Logs> logs { get; set; }
        public DbSet<Accounts> accounts { get; set; }
        public DbSet<Reports> reports { get; set; }


    }
}
