﻿// <auto-generated />
using System;
using LapTrinhWebBuoi7.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace LapTrinhWebBuoi7.Migrations
{
    [DbContext(typeof(ApplicationDBContext))]
    [Migration("20240401031844_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Accounts", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("AccountName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("CustomerId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("accounts");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Contact")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Employees", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Contact")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("employees");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Logs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("LoginDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("LoginTime")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TransactionalId")
                        .HasColumnType("int");

                    b.Property<int?>("transactionsId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("transactionsId");

                    b.ToTable("logs");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Reports", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("AccountId")
                        .HasColumnType("int");

                    b.Property<int>("LogsId")
                        .HasColumnType("int");

                    b.Property<DateTime>("ReportDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("ReportName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TransactionalId")
                        .HasColumnType("int");

                    b.Property<int?>("accountsId")
                        .HasColumnType("int");

                    b.Property<int?>("transactionsId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("LogsId");

                    b.HasIndex("accountsId");

                    b.HasIndex("transactionsId");

                    b.ToTable("reports");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Transactions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("CustomerId")
                        .HasColumnType("int");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("employeesId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("employeesId");

                    b.ToTable("transactions");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Accounts", b =>
                {
                    b.HasOne("LapTrinhWebBuoi7.Models.Customer", "customer")
                        .WithMany("accounts")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("customer");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Logs", b =>
                {
                    b.HasOne("LapTrinhWebBuoi7.Models.Transactions", "transactions")
                        .WithMany("logs")
                        .HasForeignKey("transactionsId");

                    b.Navigation("transactions");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Reports", b =>
                {
                    b.HasOne("LapTrinhWebBuoi7.Models.Logs", "logs")
                        .WithMany("reports")
                        .HasForeignKey("LogsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LapTrinhWebBuoi7.Models.Accounts", "accounts")
                        .WithMany("reports")
                        .HasForeignKey("accountsId");

                    b.HasOne("LapTrinhWebBuoi7.Models.Transactions", "transactions")
                        .WithMany("reports")
                        .HasForeignKey("transactionsId");

                    b.Navigation("accounts");

                    b.Navigation("logs");

                    b.Navigation("transactions");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Transactions", b =>
                {
                    b.HasOne("LapTrinhWebBuoi7.Models.Customer", "customer")
                        .WithMany("transactions")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LapTrinhWebBuoi7.Models.Employees", "employees")
                        .WithMany("transactions")
                        .HasForeignKey("employeesId");

                    b.Navigation("customer");

                    b.Navigation("employees");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Accounts", b =>
                {
                    b.Navigation("reports");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Customer", b =>
                {
                    b.Navigation("accounts");

                    b.Navigation("transactions");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Employees", b =>
                {
                    b.Navigation("transactions");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Logs", b =>
                {
                    b.Navigation("reports");
                });

            modelBuilder.Entity("LapTrinhWebBuoi7.Models.Transactions", b =>
                {
                    b.Navigation("logs");

                    b.Navigation("reports");
                });
#pragma warning restore 612, 618
        }
    }
}
