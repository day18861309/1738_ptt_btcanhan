﻿namespace LapTrinhWebBuoi7.Models
{
    public class Accounts
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public string AccountName { get; set; }

        public List<Reports>? reports { get; set; }

        public Customer? customer { get; set; }
    }
}
