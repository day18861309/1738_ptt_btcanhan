﻿namespace LapTrinhWebBuoi7.Models
{
    public class Transactions
    {
        public int Id {  get; set; }

        public int EmployeeId { get; set;}

        public int CustomerId { get; set; }

        public string Name { get; set; }

        public Employees? employees { get; set; }

        public Customer? customer { get; set; }

        public List<Reports>? reports { get; set; }
        public List<Logs>? logs { get; set; }
    }
}
