﻿namespace LapTrinhWebBuoi7.Models
{
    public class Employees
    {
        public int Id { get; set; }
        public string FirstName {  get; set; }
        public string LastName { get; set; }

        public string Contact {  get; set; }

        public string Address { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public List<Transactions>? transactions { get; set; }
    }
}
