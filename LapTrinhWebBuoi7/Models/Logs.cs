﻿namespace LapTrinhWebBuoi7.Models
{
    public class Logs
    {
        public int Id { get; set; }

        public int TransactionalId { get; set; }

        public DateTime LoginDate { get; set; } 

        public string LoginTime { get; set; }

        public Transactions? transactions {  get; set; }

        public List<Reports>? reports { get; set; }
    }
}
