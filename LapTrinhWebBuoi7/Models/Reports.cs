﻿namespace LapTrinhWebBuoi7.Models
{
    public class Reports
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public int LogsId { get; set; }

        public int TransactionalId { get; set; }

        public string ReportName { get; set; }  

        public DateTime ReportDate { get; set; }

        public Accounts? accounts { get; set; }

        public Logs? logs { get; set; }

        public Transactions? transactions { get; set; }

    }
}
